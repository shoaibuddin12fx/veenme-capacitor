import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { NavService } from './services/basic/nav.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(
    private user: UserService,
    private nav: NavService,
    private menu: MenuController
  ) {
    this.checkLogin();
  }

  checkLogin() {
    this.user.getToken().then((token) => {
      if (token) {
        this.nav.setRoot('home');
      }
    })
  }

  goTo(route) {
    this.nav.setRoot(route);
    this.menu.close();
  }

  logOut() {
    localStorage.removeItem('token');
    this.menu.close();
    this.nav.setRoot('register');
  }
}
