import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { ExploreItemComponent } from './explore-item/explore-item.component';

@NgModule({
    declarations: [
        HeaderComponent,
        ExploreItemComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
    ],
    exports: [
        HeaderComponent,
        ExploreItemComponent
    ]
})
export class ComponentsModule { }
