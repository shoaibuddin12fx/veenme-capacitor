import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent extends BasePage implements OnInit {

  @Input('item') item;
  comment = ''
  list = [];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.randomListGenerate();
  }

  randomListGenerate(){

    for(var i = 0; i < this.item.comment_count; i++){
      let p = this.makeid(5);
      this.list.push({
        avatar: p.charAt(0),
        name: p
      })

    }
  }

  makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

  close(){
    this.modals.dismiss({data: 'A'});
  }

  addComment(){

    if(!this.comment) { return };

    this.list.push({
      avatar: this.comment.charAt(0),
      name: this.comment
    });

    this.comment = '';
  }

}
