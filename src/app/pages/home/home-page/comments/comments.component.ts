import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent extends BasePage implements OnInit {

  @Input('item') item;
  comment = ''
  list = [];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.getComments();
  }

  async getComments(){
    console.log(this.item);
   let response = await this.network.getPostComments(this.item.id);
   this.list = response.data.item;
  }

  // randomListGenerate(){

  //   for(var i = 0; i < this.item.comment_count; i++){
  //     let p = this.makeid(5);
  //     this.list.push({
  //       avatar: p.charAt(0),
  //       name: p
  //     })

  //   }
  // }

  makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

  close(){
    this.modals.dismiss({data: 'A'});
  }

  async likeUnlikeComment(item){
    let response = await this.network.likeUnlikePostComment(item.post_id,item.id);
    if(response && response.success === true){
      item.is_like = !item.is_like;
      if(item.is_like === true) item.like_count++;
      else item.like_count--;
    }
  }

  async addComment(){
    if(!this.comment) return;
    let data = {post_id: this.item.id, text: this.comment};
    let response = await this.network.createComment(data);
    console.log(response);
    
    if(response && response.success === true){
      this.utility.alerts.presentToast("Comment successfully added");
      this.modals.dismiss({data: data});
    }
        // this.list.push({
    //   avatar: this.comment.charAt(0),
    //   name: this.comment
    // });

    this.comment = '';
  }

}
