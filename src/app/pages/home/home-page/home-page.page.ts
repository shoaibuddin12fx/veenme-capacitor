import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import { CommentsComponent } from './comments/comments.component';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.page.html',
  styleUrls: ['./home-page.page.scss'],
})
export class HomePagePage extends BasePage implements OnInit {

  page = 1;
  list = [];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  // ionViewWillEnter(){
  //   this.initialize();
  // }

  async initialize() {
    const res = await this.network.getPublicPosts(this.page);
    console.log(res)
    let data = res.data
    this.list = data.item;
  }

  async itemLikeToggled(item){
    let response = await this.network.likeUnlikePost(item.id);
    if(response && response.success === true){
      item.is_like = !item.is_like;
      if(item.is_like === true) item.like_count++;
      else item.like_count--;
    }
      
  }

  openComments(item){
    this.modals.present(CommentsComponent, {item}).then(res => {
      this.initialize();
    });
  }

}
