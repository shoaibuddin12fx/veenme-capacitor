import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-places-details',
  templateUrl: './places-details.component.html',
  styleUrls: ['./places-details.component.scss'],
})
export class PlacesDetailsComponent extends BasePage implements OnInit {

  @ViewChild('content') content: IonContent;
  @Input('item') item;
  reviews = [];
  page = 1;
  detail;
  mapimage = '';
  list = [];

  constructor(injector: Injector) {
    super(injector);

  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {

    const res = await this.network.getPlacesById(this.item.place_id);
    console.log(res);
    let data = res.data;
    this.detail = data;
    this.reviews = data.reviews;
    let scrollElement = await this.content.getScrollElement();
    console.log("clientWidth: ", scrollElement.clientWidth);
    let w = scrollElement.clientWidth;
    let h = parseInt( (w/2).toString(), 10);
    this.mapimage = `https://maps.googleapis.com/maps/api/staticmap?markers=${this.item.latitude},${this.item.longitude}&size=${w}x${h}&key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA`
    console.log(this.mapimage);

    const posts = await this.network.getPublicPostsByPlaceId(this.item.place_id);
    this.list = posts.data.item;
  }

  close(){
    this.modals.dismiss({ data: 'A'});
  }

}
