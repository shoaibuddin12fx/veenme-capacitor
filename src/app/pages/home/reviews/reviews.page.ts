import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.page.html',
  styleUrls: ['./reviews.page.scss'],
})
export class ReviewsPage extends BasePage implements OnInit {

  page = 1;
  list = [];
  constructor(injector: Injector) {
    super(injector);

  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {

    const res = await this.network.getReiews(this.page);
    console.log(res)
    let data = res.data
    this.list = data.item;

  }

}
