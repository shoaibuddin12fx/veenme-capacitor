import { Component, Injector } from '@angular/core';
import { Browser } from '@capacitor/browser';
import { FirebaseAuthentication } from '@robingenz/capacitor-firebase-authentication';
import { BasePage } from '../base-page/base-page';
import { LoginPage } from '../login/login.page';
import { SignupPage } from '../signup/signup.page';

@Component({
  selector: 'app-register',
  templateUrl: 'register.page.html',
  styleUrls: ['register.page.scss'],
})
export class RegisterPage extends BasePage {

  // step = 1;

  constructor(injector: Injector) {
    super(injector)
  }

  async loginWithGoogle() {
    try {
      const res = await FirebaseAuthentication.signInWithGoogle();
      const data = res.user;
      if (data) {
        this.signUpwithSocial(res);

      }
    } catch (err) {
      console.error(err);
    }
  }

  async loginWithFacebook() {
    try {
      const res = await FirebaseAuthentication.signInWithFacebook();
      console.log(res);
      if (res) {
        this.signUpwithSocial(res);
        // await Browser.open({ url: `https://dev-veenme.thesupportonline.net/testtoken/${token}` });
      }
    } catch (err) {
      console.error(err);
    }
  }

  async openLogin() {
    const res = await this.modals.present(LoginPage);
    this.nav.setRoot('home');
  }

  async openSignup() {
    const res = await this.modals.present(SignupPage);
    this.nav.setRoot('home');
  }

  signUpwithSocial(data) {
    return new Promise(async resolve => {
      console.log(data);
      let obj = {
        fname: '',
        lname: '',
        email: '',
        phone: '+1',
        dob: '',
        gender: ''

      }

      //       fname: Shees
      // lname: Ali
      // email: sheessyed101@gmail.com
      // platform: web
      // dob:
      // dob: 1995-04-30
    })
  }
}
