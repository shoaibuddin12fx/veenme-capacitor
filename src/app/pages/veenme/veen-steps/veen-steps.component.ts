import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-veen-steps',
  templateUrl: './veen-steps.component.html',
  styleUrls: ['./veen-steps.component.scss'],
})
export class VeenStepsComponent extends BasePage implements OnInit {

  step = 1;
  activities: any;

  constructor(
    injector: Injector
  ) {
    super(injector)
  }

  async ngOnInit(): Promise<void> {
    await this.getActivities();
  }

  async getActivities(): Promise<any> {
    const { data } = await this.network.getActivities();
    this.activities = data.item;
    console.log(this.activities);
  }

  close() {
    this.modals.dismiss();
  }
}
