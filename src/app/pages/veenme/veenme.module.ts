import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VeenmePageRoutingModule } from './veenme-routing.module';

import { VeenmePage } from './veenme.page';
import { VeenStepsComponent } from './veen-steps/veen-steps.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VeenmePageRoutingModule
  ],
  declarations: [VeenmePage, VeenStepsComponent]
})
export class VeenmePageModule {}
